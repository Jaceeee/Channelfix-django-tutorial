from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'registration'

urlpatterns = [
    url('login/', auth_views.login, name='login'),
    url('logout/', auth_views.logout,
        {'template_name': 'registration/logged_out.html'}, name='logout'),
    url('signup/', views.signup, name='signup'),
    url('signup_success/', views.signup_success, name='signup_success')
]
