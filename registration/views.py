from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse

from registration.forms import UserLoginForm, SignUpForm

# Create your views here.


class LoginView(FormView):
    template_name = 'registration/login.html'
    form_class = UserLoginForm
    success_url = '/thanks/'

    def form_valid(self, form):
        return HttpResponseRedirect(reverse('polls:index'))


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse('registration:signup_success'))
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})
# def logout_confirmation(request):
#     return render(request, 'registration/logout_confirmation.html')


def signup_success(request):
    return render(request, 'registration/signup_success.html')
