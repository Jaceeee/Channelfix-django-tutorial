from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Question, Choice

from polls.forms import CreateQuestionForm


class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last 5 published questions (not including
        those set to be published in the future).
        """

        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]


class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(LoginRequiredMixin, generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def create_question(request):
    if request.method == 'POST':
        form = CreateQuestionForm(request.POST)
        
        question = Question()
        question.question_text = request.POST['question_text']
        question.pub_date = timezone.now()
        question.save()

        choices = [Choice(choice_text=request.POST['choice1']),
                   Choice(choice_text=request.POST['choice2']),
                   Choice(choice_text=request.POST['choice3'])
                   ]

        for c in choices:
            c.question_id = question.id
            c.save()
            question.choice_set.add(c)

        return redirect(reverse('polls:index'))
    else:
        form = CreateQuestionForm()
        return render(request, 'polls/create_question.html', {'form': form})


def add_choice_to_question(request, question_id):
    return render(request, 'polls/%s/create_question.html' %(question_id))


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })

    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect with the saved POST data
        return HttpResponseRedirect(
            reverse('polls:results', args=(question.id,)))


def logout_view(request):
    logout(request)
    # redirect to success page
    # return HttpResponseRedirect(reverse('polls:login'))
