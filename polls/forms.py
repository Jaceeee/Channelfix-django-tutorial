from django import forms


class CreateQuestionForm(forms.Form):
    question_text = forms.CharField(required=True)
